# twostep-python 
[![Build Status](https://travis-ci.org/objectia/twostep-python.svg?branch=master)](https://travis-ci.org/objectia/twostep-python)
[![codecov](https://codecov.io/gh/objectia/twostep-python/branch/master/graph/badge.svg)](https://codecov.io/gh/objectia/twostep-python)
[![Python](https://img.shields.io/pypi/pyversions/setuptools.svg)]()


Python API client for twostep.io

The Twostep Python library provides convenient access to the twostep.io API from
applications written in the Python language. 

## Documentation

See the [Python API docs](https://docs.twostep.io/api/index.html).

## Installation

You don't need this source code unless you want to modify the package. If you just
want to use the package, just run:

    pip install --upgrade twostep

or

    easy_install --upgrade twostep

Install from source with:

    python setup.py install

### Requirements

* Python 2.7 or Python 3.4+

## Usage

The library needs to be configured with your account's client id and client secret:

``` python
import twostep

client_id = "<your client id>"
client_secret = "<your client secret>"

client = twostep.Client(client_id=client_id, client_secret=client_secret)

# create a new user
user = client.create_user("jdoe@example.com", "+12125551234", 1)
```
