import requests
import logging
import json

try:
    from urllib import quote
except ImportError:
    from urllib.parse import quote

from twostep.errors import APIConnectionError, APITimeoutError, ResponseError
from twostep.token import Token
from twostep.tokenstore import TokenStore
from twostep.version import VERSION

LIVE_API_URL = "https://api.twostep.io"
SANDBOX_API_URL = "http://localhost:4000"  # "https://sandbox-api.twostep.io"

DEFAULT_TIMEOUT = 30  # seconds
USER_AGENT = "twostep-python/{0}".format(VERSION)


class AbstractClient:
    def __init__(self, **kwargs):
        """
        @param kwargs: Optional parameters
        """
        self.client_id = kwargs.get("client_id", None)
        self.client_secret = kwargs.get("client_secret", None)

        if not self.client_id:
            raise ValueError("No Client ID provided")
        if not self.client_secret:
            raise ValueError("No Client secret provided")

        self.api_url = kwargs.get("api_url", LIVE_API_URL)

        self.sandbox = kwargs.get("sandbox", None)
        if self.sandbox:
            self.api_url = SANDBOX_API_URL

        self.timeout = kwargs.get("timeout", DEFAULT_TIMEOUT)
        self.token_store = kwargs.get("token_store", None)

    def get(self, path):
        """
        Execute a HTTP GET
        """
        return self.request_with_token("GET", path)

    def post(self, path, data):
        """
        Execute a HTTP POST
        """
        return self.request_with_token("POST", path, data)

    def put(self, path, data):
        """
        Execute a HTTP PUT
        """
        return self.request_with_token("PUT", path, data)

    def delete(self, path):
        """
        Execute a HTTP DELETE
        """
        return self.request_with_token("DELETE", path)

    def request_with_token(self, method, path, data={}):
        """
        Obtain a token and execute HTTP request
        """
        token = self.get_token()
        return self.request(method, path, data, token)

    def request(self, method, path, data={}, token=None):
        """
        Execute a HTTP request
        """
        logging.debug("Sending HTTP request")

        url = self.api_url + quote(path)

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "User-Agent": USER_AGENT
        }
        if isinstance(token, Token):
            headers["Authorization"] = "Bearer {0}".format(token.access_token)

        try:
            params = {}
            payload = None
            if method == "GET":
                params.update(data)
            elif method in ["POST", "PUT"]:
                payload = json.dumps(data)
            resp = requests.request(method, url, headers=headers, params=params, data=payload, timeout=self.timeout)
        except requests.exceptions.Timeout as e:
            raise APITimeoutError("Request timed out: " + repr(e))
        except requests.exceptions.RequestException as e:
            raise APIConnectionError("Unable to connect to server: " + repr(e))

        if resp.status_code not in [200, 201]:
            try:
                content = resp.json()
                message = content["message"]
            except (KeyError, ValueError):
                # Not JSON response, or message set
                if resp.status_code == 501:
                    message = "Not implemented"
                elif resp.status_code == 502:
                    message = "Bad gateway"
                elif resp.status_code == 503:
                    message = "Service unavailable"
                elif resp.status_code == 504:
                    message = "Gateway timeout"
                else:
                    message = "Internal server error"
            raise ResponseError(resp.status_code, message)
        return resp

    def get_token(self):
        """
        Return a token to the caller
        """
        token = None
        # first try get from token store
        if isinstance(self.token_store, TokenStore):
            token = self.token_store.load()

        if isinstance(token, Token) and token.isValid():
            if token.isExpired():
                return self.refresh_token(token)
            else:
                return token
        return self.new_token()

    def new_token(self):
        """
        Get a new token from the server
        """
        data = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "grant_type": "client_credentials"
        }
        resp = self.request("POST", "/auth/token", data)
        token = Token.fromJSON(self, resp)

        if isinstance(self.token_store, TokenStore):
            self.token_store.save(token)

        return token

    def refresh_token(self, old_token):
        """
        Get a new tokenfrom server using a refresh token
        """
        data = {
            "client_id": self.client_id,
            "client_secret": self.client_secret,
            "grant_type": "refresh_token",
            "refresh_token": old_token.refresh_token
        }
        resp = self.request("POST", "/auth/token", data)
        token = Token.fromJSON(self, resp)

        if isinstance(self.token_store, TokenStore):
            self.token_store.save(token)

        return token
