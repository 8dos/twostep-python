from abc import ABCMeta, abstractmethod


class TokenStore:
    """
    Token storage that must be implemented by the caller of the client in order to store
    the access token (and refresh token) between sessions.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def load(self):
        """
        Load a token from store
        """
        raise NotImplementedError

    @abstractmethod
    def save(self, token):
        """
        Save a token to store
        """
        raise NotImplementedError

    @abstractmethod
    def clear(self):
        """
        Empty the store
        """
        raise NotImplementedError
