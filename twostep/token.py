import jwt
import time


class Token:
    """
    Access token passed from server to client, which is sent back to server with each request.
    """
    def __init__(self):
        self.resource = None
        self.response = None
        self.access_token = None
        self.token_type = None
        self.scope = None
        self.expires_in = None
        self.refresh_token = None

    @classmethod
    def fromJSON(cls, resource, response):
        token = Token()
        token.resource = resource
        token.response = response
        try:
            content = token.response.json()
            if (isinstance(content, dict)):
                token.access_token = content["access_token"]
                token.token_type = content["token_type"]
                token.scope = content["scope"]
                token.expires_in = content["expires_in"]
                token.refresh_token = content["refresh_token"]
        except (ValueError, AttributeError):
            pass
        return token

    def isValid(self):
        """
        Check if token is valid (ie. has access and refresh token)
        """
        if not self.access_token or not self.refresh_token:
            return False
        return True

    def isExpired(self):
        """
        Check if an access token has expired
        """
        try:
            decoded = jwt.decode(self.access_token, verify=False)
            expires_at = decoded["exp"]
            now = int(time.time())
            if expires_at > now:
                return False
        except (KeyError, UnicodeDecodeError, jwt.DecodeError):
            pass  # payload is not valid
        return True
