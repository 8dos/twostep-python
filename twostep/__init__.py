from twostep.base import AbstractClient
from twostep.models import User, Sms, Call


class Client(AbstractClient):
    """
    Client for sending requests to twostep.io server
    """

    def create_user(self, email, phone, country_code, send_install_link=False):
        data = {
            "email": email,
            "phone": phone,
            "country_code": country_code,
            "send_install_link": send_install_link
        }
        resp = self.post("/v1/users", data)
        return User(self, resp)

    def get_user(self, user_id):
        resp = self.get("/v1/users/{0}".format(user_id))
        return User(self, resp)

    def request_sms(self, user_id, options={}):  # FIXME: options?
        resp = self.post("/v1/users/{0}/sms".format(user_id), options)
        return Sms(self, resp)

    def request_call(self, user_id, options={}):  # FIXME: options?
        resp = self.post("/v1/users/{0}/call".format(user_id), options)
        return Call(self, resp)

    def remove_user(self, user_id):
        resp = self.delete("/v1/users/{0}".format(user_id))
        return User(self, resp)
