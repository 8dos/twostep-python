Python API client for twostep.io
================================

A Python library for the twostep.io API.


Setup
-----

You can install this package by using the pip tool and installing:

    $ pip install twostep
    
Or:

    $ easy_install twostep
    

Setting up a twostep.io account
-------------------------------

Sign up for Twostep at https://twostep.io.


Using the twostep.io API
------------------------

Documentation for the Python client can be found alongside the other API clients here:

- https://docs.twostep.io/api/index.html
