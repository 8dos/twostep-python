import unittest
import tests.config as config
import twostep
from twostep.errors import ResponseError
from twostep.models import User, Sms, Call
from twostep.token import Token


class ClientTest(unittest.TestCase):

    def setUp(self):
        store = config.MyTokenStore()
        self.client = twostep.Client(client_id="12345", client_secret="67890", sandbox=True, token_store=store)

    def test_api_without_client_id(self):
        with self.assertRaises(ValueError):
            twostep.Client(client_id="", client_secret="67890", sandbox=True)

    def test_api_without_client_secret(self):
        with self.assertRaises(ValueError):
            twostep.Client(client_id="12345", client_secret="", sandbox=True)

    """
    def test_user_model(self):
        user = User()
        self.assertIsInstance(user, User)
"""
    def test_token(self):
        token = Token()
        self.assertIsInstance(token, Token)

    def test_token_valid(self):
        token = Token()
        token.access_token = "access-token"
        token.refresh_token = "refresh-token"
        self.assertEqual(True, token.isValid())

    def test_token_expired(self):
        token = Token()
        token.access_token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A"
        self.assertEqual(True, token.isExpired())

    def test_token_expired_invalid(self):
        token = Token()
        token.access_token = "invalid.payload.here"
        self.assertEqual(True, token.isExpired())

    def test_token_invalid(self):
        token = Token()
        self.assertEqual(False, token.isValid())

    def test_create_user(self):
        user = self.client.create_user("jdoe@example.com", config.PHONE_NUMBER, config.COUNTRY_CODE)
        self.assertIsInstance(user, User)

    def test_create_user_with_invalid_email(self):
        with self.assertRaises(ResponseError):
            self.client.create_user("not-an-email", config.PHONE_NUMBER, config.COUNTRY_CODE)

    def test_get_user(self):
        user = self.client.get_user("123456")
        self.assertIsInstance(user, User)
        self.assertEqual("123456", user.id)

    def test_get_unknown_user(self):
        with self.assertRaises(ResponseError):
            self.client.get_user("000000")

    def test_get_user_with_expired_token(self):
        store = config.ExpiredTokenStore()
        cli = twostep.Client(client_id="12345", client_secret="67890", sandbox=True, token_store=store)
        user = cli.get_user("123456")
        self.assertIsInstance(user, User)
        self.assertEqual("123456", user.id)

    def test_get_user_with_bad_token(self):
        store = config.BadTokenStore()
        cli = twostep.Client(client_id="12345", client_secret="67890", sandbox=True, token_store=store)
        user = cli.get_user("123456")
        self.assertIsInstance(user, User)
        self.assertEqual("123456", user.id)

    def test_request_sms(self):
        sms = self.client.request_sms("123456")
        self.assertIsInstance(sms, Sms)
        # not found
        with self.assertRaises(ResponseError):
            self.client.request_sms("000000")

    def test_request_sms_unknown_user(self):
        with self.assertRaises(ResponseError):
            self.client.request_sms("000000")

    def test_request_call(self):
        sms = self.client.request_call("123456")
        self.assertIsInstance(sms, Call)

    def test_request_call_unknown_user(self):
        with self.assertRaises(ResponseError):
            self.client.request_call("000000")

    def test_remove_user(self):
        user = self.client.remove_user("123456")
        self.assertIsInstance(user, User)

    def remove_unknown_user(self):
        with self.assertRaises(ResponseError):
            self.client.remove_user("000000")


if __name__ == "__main__":
    unittest.main()
