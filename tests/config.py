import os
import logging
from twostep.token import Token
from twostep.tokenstore import TokenStore

# Set log level for all unit tests
logging.basicConfig(level=logging.DEBUG, format="%(levelname)s: %(message)s")

PHONE_NUMBER = os.environ.get("TWOSTEP_TEST_PHONE_NUMBER", "+37000000000000")
COUNTRY_CODE = 370


class MyTokenStore(TokenStore):
    def __init__(self):
        self.token = None

    def load(self):
        logging.debug("Load token from store...")
        return self.token

    def save(self, token):
        logging.debug("Save token to store...")
        self.token = token

    def clear(self):
        logging.debug("Clear store...")
        self.token = None


class ExpiredTokenStore(TokenStore):
    def load(self):
        token = Token()
        token.access_token = "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjEyMzQ1NiJ9.eyJpc3MiOiJ0d29zdGVwLmlvIiwiaWF0IjoxNTIzMDk3ODYwLCJleHAiOjE1MjMxMDE0NjAsImF1ZCI6IjEyMzQ1Iiwic2NvcGUiOiJub25lIn0.gfGo7AS6GiJU5HE7buuU_c6-_WD6OC-Gf3ZUTY9Xhp7x8qY9LLK1S-LuUD8EXh6krzLtHZU2en7Z2JVo2VK1ynXKGnh3vGIlAogHtL71y-ecs4SIdsPfEp3y3WPckE9gIkA5DANLkxl5XfdN1UV5BX0ULKH3zfzBtrGQnuiXa3J7vqBq66hLkc8R7kCJv0hEjhkQ54p6QgSS3AIO3TSSmgi8B7Yg6oswszu5Y4X9LR29S9-Tb-gfrDzfT134GWjDJifjNrS7y-SSnpT9nUOgTeXhXpcgFKIRtwZMOt9ptugBk3WAkm_kpQhy_Dw92HFnCGyWQkENqPujRmgnZRn01A"
        token.token_type = "Bearer"
        token.scope = "none"
        token.expires_in = 3600
        token.refresh_token = "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f"
        return token

    def save(self, token):
        pass

    def clear(self):
        pass


class BadTokenStore(TokenStore):
    def load(self):
        token = Token()
        token.access_token = "this.willnot.work"
        token.token_type = "Bearer"
        token.scope = "none"
        token.expires_in = 3600
        token.refresh_token = "0e7ce3f5-1d78-4f2f-902f-8386eaedb50f"
        return token

    def save(self, token):
        pass

    def clear(self):
        pass
